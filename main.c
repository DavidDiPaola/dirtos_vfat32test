#include <stdio.h>

#include <stdlib.h>

#include "common.h"

#include "disk.h"

#include "mbr.h"

#include "utf8.h"

/* VFAT32 documents:
	- https://www.pjrc.com/tech/8051/ide/fat32.html
	- https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system#VFAT
*/

#define DEBUGP(fmt, ...) printf("DEBUG[%s:%s():%i] " fmt, __FILE__, __func__, __LINE__, ##__VA_ARGS__)
//#define DEBUG_STATE_READ
//#define DEBUG_INIT
//#define DEBUG_GETSTART
//#define DEBUG_CHAIN_READ
//#define DEBUG_DIRLOOKUPC
//#define DEBUG_DIRRECORDGETNAME
//#define DEBUG_PATHLOOKUPC

/* must be at least as large as the maximum physical sector size */
#define vfat32_logicalsectorsize_MAX 4096

struct vfat32_state {
	/* stuff from the disk */
	u32f sectorsize_b;  /* [physical] sector size (in bytes) */
	/* stuff from the partition table */
	u32f partitionstart_ps;  /* start location (in physical sectors) */
	/* stuff from BPB */
	u32f fatstart_ps;       /* FAT start location (in physical sectors) */
	u8f  fats;              /* number of FATs */
	u32f fatsize_ps;        /* FAT size (in physical sectors) */
	u8f  clustersize_ps;    /* cluster size (in physical sectors) */
	u32f clustersstart_ps;  /* cluster area start (in physical sectors) */
	u32f rootdir_c;         /* root dir cluster number */
	/* FAT */
	size   fat_length;    /* FAT length (in u32) */
	u32  * fat;           /* in-memory FAT */
	u32f   fat_chainend;  /* FAT end-of-chain marker */
};

struct vfat32_file {
	u32f first_c;  /* file contents' first cluster */
	u32f size_b;   /* file contents' size */
	struct {       /* current seek position */
		u32f cluster;  /* current cluster number */
		u32f offset;   /* offset into the current cluster */
	} position;
};

struct __attribute__((packed)) _bpb {
	u8 _pad[0x0B];
	/* DOS 2.0 BIOS Parameter Block (see: https://en.wikipedia.org/wiki/BIOS_parameter_block#DOS_2.0_BPB) */
	u16 logicalsectorsize_b;  /* logical sector size (in bytes) */
	u8  clustersize_ls;       /* cluster size (in logical sectors) */
	u16 reserved_ls;          /* reserved sectors (in logical sectors) */
	u8  fats;                 /* number of FATs */
	u16 _rootdirentries;      /* number of entries in the root dir */
	u16 _size16_ls;           /* size (in logical sectors) (unused in this code) */
	u8  _descriptor;          /* media descriptor (unused in this code) */
	u16 _fatsize_ls;          /* FAT size (in logical sectors) */
	/* DOS 3.31 BIOS Parameter Block (see: https://en.wikipedia.org/wiki/BIOS_parameter_block#DOS_3.31_BPB) */
	u16 _tracksize_ps;   /* */
	u16 _heads;          /* */
	u32 _hiddensectors;  /* */
	u32 _size32_ls;      /* */
	/* DOS 7.1 Extended BIOS Parameter Block (see: https://en.wikipedia.org/wiki/BIOS_parameter_block#DOS_3.31_BPB) */
	u32 fatsize_ls;
	u16 _flags_mirroring;
	u16 _version;
	u32 rootdir_c;
	u16 _fsinfo_ls;  /* TODO confirm its ls */
	u16 _backup_ls;  /* TODO confirm its ls */
	u8  _bootfilename[12];
	u8  _drive;  /* physical drive number */
	u8  _flags;
	u8  _bootsig_ext;
	u32 _serial;  /* volume serial number */
	u8  _label[11];  /* volume label */
	u8  _fstype[8];
};

struct _dir_record {
	union {
		struct __attribute__((packed)) {
			char name[8];       /* 0x00 */
			char extension[3];  /* 0x08 */
			u8   attributes;    /* 0x0B */
			u8   _pad0[0x14-(0x0B+sizeof(u8))];
			u16  start_high_c;  /* 0x14 */
			u8   _pad1[0x1A-(0x14+sizeof(u16))];
			u16  start_low_c;   /* 0x1A */
			u32  size_b;        /* 0x1C */
		} normal;

		struct __attribute__((packed)) {
			u8  sequence;    /* 0x00 */
			u16 name0[5];    /* 0x01 */
			u8  attributes;  /* 0x0B */
			u8  type;        /* 0x0C */
			u8  checksum;    /* 0x0D */
			u16 name1[6];    /* 0x0E */
			u16 start_c;     /* 0x1A */
			u16 name2[2];    /* 0x1C */
		} lfn;
	};
};
#define _dir_record_ISENDOFDIR(r_ptr)  ((r_ptr)->normal.name[0] == 0x00)
#define _dir_record_ISEMPTY(r_ptr)     ((r_ptr)->normal.name[0] == 0xE5)
#define _dir_record_ISREADONLY(r_ptr)  ((r_ptr)->normal.attributes & (1 << 0))
#define _dir_record_ISHIDDEN(r_ptr)    ((r_ptr)->normal.attributes & (1 << 1))
#define _dir_record_ISSYSTEM(r_ptr)    ((r_ptr)->normal.attributes & (1 << 2))
#define _dir_record_ISVOLUMEID(r_ptr)  ((r_ptr)->normal.attributes & (1 << 3))
#define _dir_record_ISDIRECTORY(r_ptr) ((r_ptr)->normal.attributes & (1 << 4))
#define _dir_record_ISARCHIVE(r_ptr)   ((r_ptr)->normal.attributes & (1 << 5))
#define _dir_record_ISLFN(r_ptr)       (((r_ptr)->normal.attributes & 0xF) == 0xF)

static void
_debug_ucs2_print(const u16 * str, size str_length) {
	/* TODO i guess UCS2 is UTF-16 but only 2byte */
	for (size i=0; i<str_length; i++) {
		u16 c = str[i];
		if ((c > 0x007F) || (c == 0x0000)) {
			printf("{0x%04X}", c);
		}
		else {
			printf("%c", (u8)c);
		}
	}
}

static void
_debug_dir_record_normal_print(const struct _dir_record * r, const char * lineprefix) {
	printf("%s" "name: \"%.*s\""            "\n", lineprefix, (int)sizeof(r->normal.name), r->normal.name);
	printf("%s" "extension: \"%.*s\""       "\n", lineprefix, (int)sizeof(r->normal.extension), r->normal.extension);
	printf("%s" "attributes: 0x%02X"        "\n", lineprefix, r->normal.attributes);
	printf("%s" "start: logical cluster %u" "\n", lineprefix, (r->normal.start_high_c << 16) | r->normal.start_low_c);
	printf("%s" "size: %u bytes"            "\n", lineprefix, r->normal.size_b);
}

static void
_debug_dir_record_lfn_print(const struct _dir_record * r, const char * lineprefix) {
	printf("%s" "sequence: 0x%02X" "\n", lineprefix, r->lfn.sequence);
	printf("%s" "name0: \"", lineprefix);
	_debug_ucs2_print(r->lfn.name0, lengthof(r->lfn.name0));
	printf("\"\n");
	printf("%s" "attributes: 0x%02X" "\n", lineprefix, r->lfn.attributes);
	printf("%s" "type: 0x%02X"       "\n", lineprefix, r->lfn.type);
	printf("%s" "checksum: 0x%02X"   "\n", lineprefix, r->lfn.checksum);
	printf("%s" "name1: \"", lineprefix);
	_debug_ucs2_print(r->lfn.name1, lengthof(r->lfn.name1));
	printf("\"\n");
	printf("%s" "start_c: 0x%04X" "\n", lineprefix, r->lfn.start_c);
	printf("%s" "name2: \"", lineprefix);
	_debug_ucs2_print(r->lfn.name2, lengthof(r->lfn.name2));
	printf("\"\n");
}

static void
_debug_dir_print_r(const struct _dir_record * records, size records_length) {
	DEBUGP("\n");
	const struct _dir_record * r = records;
	for (size i=0; i<records_length; i++,r++) {
		if (_dir_record_ISENDOFDIR(r)) {
			printf("\t" "records[%zu]: end of dir entry\n", i);
			break;
		}
		else if (_dir_record_ISLFN(r)) {
			printf("\t" "records[%zu]: LFN entry\n", i);
			_debug_dir_record_lfn_print(r, "\t\t");
		}
		else if (_dir_record_ISVOLUMEID(r)) {
			printf("\t" "records[%zu]: volumeid entry\n", i);
		}
		else if (_dir_record_ISEMPTY(r)){
			printf("\t" "records[%zu]: empty entry\n", i);
		}
		else {
			if (_dir_record_ISDIRECTORY(r)) {
				printf("\t" "records[%zu]: directory entry\n", i);
			}
			else {
				printf("\t" "records[%zu]: file entry\n", i);
			}
			_debug_dir_record_normal_print(r, "\t\t");
		}
	}
}

static i32
_debug_sanity(struct vfat32_state * state) {
	if (sizeof(u8) != 1) {
		DEBUGP("ERROR: sizeof(u8) is %zu (!= 1)\n", sizeof(u8));
		return -1;
	}
	if (sizeof(u16) != 2) {
		DEBUGP("ERROR: sizeof(u16) is %zu (!= 2)\n", sizeof(u8));
		return -1;
	}
	if (sizeof(u32) != 4) {
		DEBUGP("ERROR: sizeof(u32) is %zu (!= 4)\n", sizeof(u8));
		return -1;
	}

	if (state->sectorsize_b == 0) {
		state->sectorsize_b = disk_sectorsize();
	}

	/* these checks need to be done after disk_init() */
	if (vfat32_logicalsectorsize_MAX < state->sectorsize_b) {
		DEBUGP("ERROR: vfat32_logicalsectorsize_MAX < state->sectorsize_b (%u < %zu)\n", vfat32_logicalsectorsize_MAX, state->sectorsize_b);
		return -1;
	}
	i32f ratio = (vfat32_logicalsectorsize_MAX / state->sectorsize_b);
	if ((state->sectorsize_b * ratio) != vfat32_logicalsectorsize_MAX) {
		DEBUGP("WARNING: vfat32_logicalsectorsize_MAX is not an even multiple of state->sectorsize_b (%u vs %zu)\n", vfat32_logicalsectorsize_MAX, state->sectorsize_b);
	}

	if (sizeof(struct _dir_record) != 32) {
		DEBUGP("ERROR: sizeof(struct _dir_record) is %zu (!= 32)\n", sizeof(struct _dir_record));
		return -1;
	}

	return 0;
}



static void
_read_c(const struct vfat32_state * state, u32f cluster, void * buffer) {
	u32f start_ps = state->clustersstart_ps + ((cluster - 2) * state->clustersize_ps);
	disk_seekread(start_ps, buffer, state->clustersize_ps);
}

static i32f
_state_read(struct vfat32_state * state) {
	i32f status;

	/* get disk info */
	state->sectorsize_b = disk_sectorsize();
	#ifdef DEBUG_STATE_READ
	DEBUGP("from disk:\n"
		"\tphysical sector size: %lu bytes\n"
		, state->sectorsize_b);
	#endif

	/* get VFAT32 partition's start sector */
	status = mbr_partitionstart(&state->partitionstart_ps);
	if (status < 0) {
		state->partitionstart_ps = 0;
	}

	/* read BIOS Parameter Block */
	if (state->sectorsize_b < sizeof(struct _bpb)) {
		return -1;  /* TODO handle this */
	}
	struct _bpb * bpb = calloc(state->sectorsize_b, 1);
	if (!bpb) {
		perror("vfat32: _state_read(): calloc(bpb)");
		return -1;
	}
	disk_seekread(state->partitionstart_ps, bpb, 1);
	#ifdef DEBUG_STATE_READ
	DEBUGP("from BPB:\n"
		"\tlogical sector size: %u bytes\n"
		"\tcluster size: %u logical sectors\n"
		"\treserved: %u logical sectors\n"
		"\tFATs: %u\n"
		"\tFAT size: %u logical sectors\n"
		"\troot dir cluster: %u\n"
		, bpb->logicalsectorsize_b, bpb->clustersize_ls, bpb->reserved_ls, bpb->fats, bpb->fatsize_ls, bpb->rootdir_c
	);
	#endif
	u32f logicalsectorsize_ps   = bpb->logicalsectorsize_b / state->sectorsize_b;
	state->fatstart_ps          = state->partitionstart_ps + (bpb->reserved_ls * logicalsectorsize_ps);
	state->fats                 = bpb->fats;
	state->fatsize_ps           = bpb->fatsize_ls * logicalsectorsize_ps;
	state->clustersize_ps       = bpb->clustersize_ls * logicalsectorsize_ps;
	state->clustersstart_ps     = state->fatstart_ps + (state->fats * state->fatsize_ps);
	state->rootdir_c            = bpb->rootdir_c;
	#ifdef DEBUG_STATE_READ
	DEBUGP("calculated:\n"
		"\tlogical sector size: %lu physical sectors\n"
		"\tFAT start: physical sector %lu\n"
		"\tFAT size: %lu physical sectors\n"
		"\tcluster size: %u physical sectors\n"
		"\tcluster area start: physical sector %lu\n"
		, logicalsectorsize_ps
		, state->fatstart_ps, state->fatsize_ps
		, state->clustersize_ps, state->clustersstart_ps
	);
	#endif
	if (bpb->logicalsectorsize_b < state->sectorsize_b) {
		return -2;  /* TODO handle logical sector size < physical sector size */
	}
	free(bpb);

	/* read FAT */
	state->fat_length = (state->fatsize_ps * state->sectorsize_b) / sizeof(*state->fat);
	state->fat = calloc(state->fat_length, sizeof(*state->fat));
	if (!state->fat) {
		perror("vfat32: init(): calloc(fat)");
		return -1;
	}
	disk_seekread(state->fatstart_ps, state->fat, state->fatsize_ps);

	return 0;
}

static i32
_chain_read_c(const struct vfat32_state * state, u32 start_c, void * * out_buffer, size * out_buffer_size) {
	void * buffer = NULL;
	size   buffer_size = 0;

	if (start_c < 2) {
		#ifdef DEBUG_CHAIN_READ
		DEBUGP("ERROR: start cluster %u is too small\n", start_c);
		#endif
		return -1;
	}
	else if (start_c >= state->fat_length) {
		#ifdef DEBUG_CHAIN_READ
		DEBUGP("ERROR: start cluster %u is too large\n", start_c);
		#endif
		return -1;
	}

	#ifdef DEBUG_CHAIN_READ
	DEBUGP("reading cluster chain starting at %u...\n", start_c);
	#endif
	size clustersize_b = state->clustersize_ps * state->sectorsize_b;
	u32 cluster = start_c;
	for (;;) {
		size   buffer_size_new = buffer_size + clustersize_b;
		void * buffer_new = realloc(buffer, buffer_size_new);
		if (!buffer_new) {
			perror("\tERROR: _readall_c(): realloc()");
			return -2;
		}
		buffer      = buffer_new;
		buffer_size = buffer_size_new;

		#ifdef DEBUG_CHAIN_READ
		DEBUGP("\treading cluster %u\n", cluster);
		#endif
		_read_c(state, cluster, ptratbyte(buffer, buffer_size-clustersize_b));

		u32 fat_entry = state->fat[cluster];
		#ifdef DEBUG_CHAIN_READ
		DEBUGP("\tentry at fat[%u]: 0x%08X\n", cluster, fat_entry);
		#endif
		fat_entry &= 0x0FFFFFFF;  /* top 4 bits are reserved */
		#ifdef DEBUG_CHAIN_READ
		DEBUGP("\tnext cluster: ");
		#endif
		if ((fat_entry >= 0x0000002) && (fat_entry <= 0xFFFFFEF)) {
			#ifdef DEBUG_CHAIN_READ
			printf("%u\n", fat_entry);
			#endif
		}
		else if ((fat_entry >= 0xFFFFFF8) && (fat_entry <= 0xFFFFFFF)) {
			#ifdef DEBUG_CHAIN_READ
			printf("none (0x%08X)\n", fat_entry);
			#endif
			break;
		}
		else {
			#ifdef DEBUG_CHAIN_READ
			printf("ERROR\n");
			#endif
			return -1;  /* TODO free() */
		}

		cluster = fat_entry;
	}

	(*out_buffer)      = buffer;
	(*out_buffer_size) = buffer_size;
	return 0;
}

static i32
_chain_read_r(const struct vfat32_state * state, u32 dir_c, struct _dir_record * * out_records, size * out_records_length) {
	struct _dir_record * records      = NULL;
	size                 records_size = 0;
	i32f status = _chain_read_c(state, dir_c, (void *)&records, &records_size);
	if (status < 0) {
		return -1;
	}
	size records_length = records_size / sizeof(*records);

	*out_records        = records;
	*out_records_length = records_length;
	return 0;
}

static u32f
_dir_record_getstart_c(const struct _dir_record * r) {
	return (r->normal.start_high_c << 16) | r->normal.start_low_c;
}

static size
_dir_record_sfnsize(const char * str, size str_size) {
	size s = str_size;
	for (; s>=1; s--) {
		if (str[s-1] != ' ') { break; }
	}

	return s;
}

static i32
_dir_record_getname_sfn(const struct _dir_record * r, char * * out_name, size * out_name_size) {
	char * name;
	size   name_size;

	i32f status;

	const char * base      = r->normal.name;
	      size   base_size = _dir_record_sfnsize(r->normal.name, sizeof(r->normal.name));
	status = utf8_copy(base, base_size, &name, &name_size);
	if (status < 0) {
		return -1;
	}

	const char * ext      = r->normal.extension;
	      size   ext_size = _dir_record_sfnsize(r->normal.extension, sizeof(r->normal.extension));
	status = utf8_concat(&name, &name_size, ext, ext_size);
	if (status < 0) {
		return -1;
	}

	#ifdef DEBUG_DIRRECORDGETNAME
	DEBUGP("SFN is: %s\n", name);
	#endif
	*out_name      = name;
	*out_name_size = name_size;
	return 0;
}

/* walk backwards and build LFN */
static i32
_dir_record_getname(const struct _dir_record * r_first, const struct _dir_record * r, char * * out_name, size * out_name_size) {
	if ((r==r_first) || !_dir_record_ISLFN(r-1)) {
		#ifdef DEBUG_DIRRECORDGETNAME
		DEBUGP("doing SFN\n");
		#endif
		return _dir_record_getname_sfn(r, out_name, out_name_size);
	}

	#ifdef DEBUG_DIRRECORDGETNAME
	DEBUGP("getting LFN for entry %zi...\n", r-r_first);
	#endif
	char * name      = NULL;
	size   name_size = 0;
	for (;;) {
		r--;
		if ((r<r_first) || !_dir_record_ISLFN(r)) {
			break;
		}

		#ifdef DEBUG_DIRRECORDGETNAME
		DEBUGP("getting LFN data from record %zi...\n", r-r_first);
		#endif
		utf8_concat_ucs2(&name, &name_size, r->lfn.name0, lengthof(r->lfn.name0));
		utf8_concat_ucs2(&name, &name_size, r->lfn.name1, lengthof(r->lfn.name1));
		utf8_concat_ucs2(&name, &name_size, r->lfn.name2, lengthof(r->lfn.name2));
		#ifdef DEBUG_DIRRECORDGETNAME
		DEBUGP("current name value: %s\n", name);
		#endif
	}
	#ifdef DEBUG_DIRRECORDGETNAME
	DEBUGP("LFN is: \"%s\"\n", name);
	#endif

	*out_name      = name;
	*out_name_size = name_size;
	return 0;
}

static i32
_dir_lookup(const struct vfat32_state * state, u32f dir_c, const char * entry_name, struct _dir_record * out_record) {
	i32f status;

	struct _dir_record * records;
	size                 records_length;
	status = _chain_read_r(state, dir_c, &records, &records_length);
	if (status < 0) {
		return -1;
	}

	#ifdef DEBUG_DIRLOOKUPC
	_debug_dir_print_r(records, records_length);
	#endif

	bool found = 0;
	struct _dir_record * r = records;
	for (size i=0; i<records_length; r++,i++) {
		if (_dir_record_ISENDOFDIR(r)) {
			#ifdef DEBUG_DIRLOOKUPC
			DEBUGP("@%zu: end of dir entry\n", i);
			#endif
			break;
		}
		else if (_dir_record_ISEMPTY(r) || _dir_record_ISVOLUMEID(r) || _dir_record_ISLFN(r)) {
			#ifdef DEBUG_DIRLOOKUPC
			if (_dir_record_ISLFN(r)) {
				DEBUGP("@%zu: skipping (lfn)\n", i);
				_debug_dir_record_lfn_print(r, "\t\t");
			}
			else if (_dir_record_ISVOLUMEID(r)) {
				DEBUGP("@%zu: skipping (volumeid)\n", i);
			}
			else {
				DEBUGP("@%zu: skipping (empty)\n", i);
			}
			#endif
			continue;
		}
		else {
			#ifdef DEBUG_DIRLOOKUPC
			_debug_dir_record_normal_print(r, "\t\t");
			#endif

			char * r_name;
			size   r_name_size;
			status = _dir_record_getname(records, r, &r_name, &r_name_size);
			if (status < 0) {
				return -1;
			}

			#ifdef DEBUG_DIRLOOKUPC
			DEBUGP("@%zu: '%s' vs '%s' ", i, entry_name, r_name);
			#endif
			if (utf8_compare(entry_name, 999, r_name, r_name_size) == 0) {
				#ifdef DEBUG_DIRLOOKUPC
				printf("yes\n");
				#endif
				//entry_c = (r->normal.start_high_c << 16) | r->normal.start_low_c;
				*out_record = *r;
				found = 1;
				break;
			}
			#ifdef DEBUG_DIRLOOKUPC
			else {
				printf("no\n");
			}
			#endif
		}
	}

	free(records);

	if (!found) {
		#ifdef DEBUG_DIRLOOKUPC
		DEBUGP("not found!\n");
		#endif
		return -1;
	}

	return 0;
}

static i32
_path_lookup(const struct vfat32_state * state, const char * const * names, size names_length, struct _dir_record * out_record) {
	struct _dir_record record;
	u32f cluster = state->rootdir_c;
	for (size i=0; i<names_length; i++) {
		#ifdef DEBUG_PATHLOOKUPC
		DEBUGP("[dir]cluster=%lu, looking for names[%zu]='%s' cluster...\n", cluster, i, names[i]);
		#endif
		i32f status = _dir_lookup(state, cluster, names[i], &record);
		if (status < 0) {
			#ifdef DEBUG_PATHLOOKUPC
			DEBUGP("ERROR\n");
			#endif
			return -1;
		}
		cluster = _dir_record_getstart_c(&record);

		#ifdef DEBUG_PATHLOOKUPC
		DEBUGP("file cluster is %lu\n", cluster);
		#endif
	}

	*out_record = record;
	return 0;
}

i32
vfat32_init(struct vfat32_state * state) {
	return _state_read(state);
}

i32
vfat32_file_open(const struct vfat32_state * state, const char * const * path, size path_length, struct vfat32_file * * out_file) {
	struct vfat32_file * file;
	i32 status;

	file = calloc(1, sizeof(*file));
	if (!file) {
		return -1;
	}

	struct _dir_record record;
	status = _path_lookup(state, path, path_length, &record);
	if (status < 0) {
		free(file);
		return -2;
	}

	file->first_c = _dir_record_getstart_c(&record);
	file->size_b  = record.normal.size_b;
	file->position.cluster = file->first_c;
	file->position.offset  = 0;
	DEBUGP("opened file\n"
		"\t" "first_c: %li" "\n"
		"\t" "size_b: %li" "\n"
		"\t" "position:" "\n"
		"\t\t" "cluster: %li" "\n"
		"\t\t" "offset: %li" "\n"
		, file->first_c
		, file->size_b
		, file->position.cluster
		, file->position.offset
	);

	(*out_file) = file;
	return 0;
}

i32
vfat32_file_read_all(const struct vfat32_state * state, const struct vfat32_file * file, void * * out_buffer, size * out_buffer_size) {
	i32 status;

	void * buffer;
	size   buffer_size;
	status = _chain_read_c(state, file->first_c, &buffer, &buffer_size);
	if (status < 0) {
		return -1;
	}

	void * b = realloc(buffer, file->size_b);
	if (!b) {
		free(buffer);
		return -2;
	}
	buffer = b;
	buffer_size = file->size_b;

	*out_buffer      = buffer;
	*out_buffer_size = buffer_size;
	return 0;
}

i32
vfat32_dir_list(const struct vfat32_state * state) {
	/* read root dir */
	DEBUGP("size of dir entry record: %zu bytes\n", sizeof(struct _dir_record));
	struct _dir_record * records = NULL;
	size             records_size = 0;
	DEBUGP("root dir cluster: %u\n", (u32)state->rootdir_c);
	_chain_read_c(state, state->rootdir_c, (void *)&records, &records_size);
	DEBUGP("records size: %zu\n", records_size);
	size records_length = records_size / sizeof(*records);

	DEBUGP("records (length: %zu):\n", records_length);
	struct _dir_record * r = records;
	for (size i=0; i<records_length; r++,i++) {
		DEBUGP("\t--- entry %zu ---\n", i);
		/* end of directory entry */
		if (_dir_record_ISENDOFDIR(r)) {
			DEBUGP("\t\tend of directory entry\n");
			break;
		}
		/* unused entry */
		else if (_dir_record_ISEMPTY(r)) {
			DEBUGP("\t\tunused entry\n");
		}
		/* long file name entry */
		else if (r->normal.attributes == 0x0F) {
			DEBUGP("\t\tLFN: \"");
			_debug_dir_record_lfn_print(r, "\t\t\t");
		}
		/* directory entry */
		else if (r->normal.attributes == 0x10) {
			u32 start_c = ((r->normal.start_high_c) << 16) | r->normal.start_low_c;
			DEBUGP(
				"\t\tname: \"%.*s\"\n"
				"\t\textension: \"%.*s\"\n"
				"\t\tattributes: 0x%02X\n"
				"\t\tstart: logical cluster %u\n"
				"\t\tsize: %u bytes\n"
				, (int)sizeof(r->normal.name), r->normal.name
				, (int)sizeof(r->normal.extension), r->normal.extension
				, r->normal.attributes
				, start_c
				, r->normal.size_b
			);
			
			char * name;
			size   name_size;
			_dir_record_getname(records, r, &name, &name_size);
			DEBUGP("\t\tLFN: '%s'\n", name);
			free(name);

		}
		/* normal entry */
		else {
			u32 start_c = ((r->normal.start_high_c) << 16) | r->normal.start_low_c;
			DEBUGP(
				"\t\tname: \"%.*s\"\n"
				"\t\textension: \"%.*s\"\n"
				"\t\tattributes: 0x%02X\n"
				"\t\tstart: logical cluster %u\n"
				"\t\tsize: %u bytes\n"
				, (int)sizeof(r->normal.name), r->normal.name
				, (int)sizeof(r->normal.extension), r->normal.extension
				, r->normal.attributes
				, start_c
				, r->normal.size_b
			);
			
			char * name;
			size   name_size;
			_dir_record_getname(records, r, &name, &name_size);
			DEBUGP("\t\tLFN: '%s'\n", name);
			free(name);

			u8   * data;
			size   data_size;
			_chain_read_c(state, start_c, (void *)&data, &data_size);
			DEBUGP("\t\tcontents: %.*s\n", (int)data_size, data);
		}
	}

	free(records);

	return 0;
}

int
main(int argc, char * * argv) {
	if (argc < 2) {
		fprintf(stderr, "syntax: %s <image file>" "\n", argv[0]);
		exit(1);
	}
	const char * argv_PATH = argv[1];

	i32 status;

	disk_init(argv_PATH);

	struct vfat32_state state;

	#if 0
	status = _debug_sanity(&state);
	if (status < 0) {
		exit(2);
	}
	#endif

	vfat32_init(&state);

	#if 0
	vfat32_dir_list(&state);
	#endif

	DEBUGP("opening file...\n");
	const char * path[] = { "a", "b", "c", "d", "e", "file.txt" };
	//const char * path[] = { "a", "doc.txt" };
	const size   path_length = lengthof(path);
	struct vfat32_file * file;
	status = vfat32_file_open(&state, path, path_length, &file);
	if (status < 0) {
		exit(2);
	}

	DEBUGP("reading file...\n");
	char * buffer;
	size   buffer_size;
	status = vfat32_file_read_all(&state, file, (void * *)(&buffer), &buffer_size);
	if (status < 0) {
		exit(2);
	}
	DEBUGP("contents:\n%s\n", buffer);
	free(buffer);

	return 0;
}

