#include <stdlib.h>  /* for calloc() */

#include "common.h"

#include "disk.h"

/* see: https://en.wikipedia.org/wiki/Master_boot_record#Sector_layout */
struct _mbr_entry {
	u8  _pad0[4];
	u8  type;
	u8  _pad1[3];
	u32 start_ps;
	u8  _pad2[4];
};
struct __attribute__((packed)) _mbr {
	u8 _pad0[446];
	struct _mbr_entry entries[4];
	u16               signature;
};

i32f
mbr_partitionstart(u32f * out_start_ps) {
	u32f start_ps;

	size sectorsize = disk_sectorsize();
	if (sectorsize < sizeof(struct _mbr)) {
		/* TODO handle small sector sizes */
		return -1;
	}

	struct _mbr * mbr = calloc(1, sectorsize);
	if (!mbr) {
		return -2;
	}

	disk_seekread(0, mbr, 1);
	if (mbr->signature != 0xAA55) {
		free(mbr);
		return -3;
	}

	bool found = 0;
	for (size i=0; (i<4)&&(!found); i++) {
		u8f type = mbr->entries[i].type;
		if ((type==0x0B) || (type==0x0C)) {
			start_ps = mbr->entries[i].start_ps;
			found = 1;
		}
	}

	free(mbr);

	if (!found) {
		return -4;
	}

	*out_start_ps = start_ps;
	return 0;
}

