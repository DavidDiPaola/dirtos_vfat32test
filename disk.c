#include <stdio.h>

#include <stdlib.h>

#include "common.h"

static struct {
	FILE * image;
	size   sectorsize;
	u32    sectormask;
} disk;

void
disk_init(const char * imagepath) {
	disk.image = fopen(imagepath, "r+");
	if (!disk.image) {
		perror(imagepath);
		exit(2);
	}

	disk.sectorsize = 512;
	disk.sectormask = ~0b111111111;
}

size
disk_sectorsize(void) {
	return disk.sectorsize;
}

void
disk_seek(size sectorno) {
	int status = fseek(disk.image, sectorno*disk.sectorsize, SEEK_SET);
	if (status < 0) {
		perror("_seek(): fseek()");
		exit(2);
	}
}

void
disk_read(void * buffer, size sectors) {
	size_t fread_amount = fread(buffer, disk.sectorsize, sectors, disk.image);
	if (fread_amount < sectors) {
		perror("read(): fread()");
		exit(2);
	}
}

void
disk_write(const void * buffer, size sectors) {
	size_t fwrite_amount = fwrite(buffer, disk.sectorsize, sectors, disk.image);
	if (fwrite_amount < sectors) {
		perror("write(): fwrite()");
		exit(2);
	}
}

void
disk_seekread(size sectorno, void * buffer, size sectors) {
	disk_seek(sectorno);
	disk_read(buffer, sectors);
}

void
disk_seekwrite(size sectorno, const void * buffer, size sectors) {
	disk_seek(sectorno);
	disk_write(buffer, sectors);
}

