#ifndef _UTF8_H
#define _UTF8_H

#include "common.h"

i32f
utf8_copy(const char * src, size src_size, char * * out_dst, size * out_dst_size);

size
utf8_length_b(const char * str, size str_size);

i32f
utf8_concat(char * * inout_str0, size * inout_str0_size, const char * str1, size str1_size);

i32f
utf8_concat_utf32_ch(char * * inout_str, size * inout_str_size, u32 ch);

i32f
utf8_concat_ucs2(char * * inout_str8, size * inout_str8_size, const u16 * str16, size str16_length);

i32f
utf8_compare(const char * str0, size str0_size, const char * str1, size str1_size);

#endif

