#ifndef _DISK_H
#define _DISK_H

#include "common.h"

#define disk_sectorsize_MIN  512
#define disk_sectorsize_MAX 4096

void
disk_init(const char * imagepath);

size
disk_sectorsize(void);

void
disk_seek(size sectorno);

void
disk_read(void * buffer, size sectors);

void
disk_write(const void * buffer, size sectors);

void
disk_seekread(size sectorno, void * buffer, size sectors);

void
disk_seekwrite(size sectorno, const void * buffer, size sectors);

#endif

