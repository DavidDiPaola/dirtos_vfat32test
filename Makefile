OBJ = main.o disk.o mbr.o utf8.o
BIN = test
_CFLAGS = -std=c99 -Wall -Wextra -fwrapv -g $(CFLAGS)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -rf $(OBJ)
	rm -rf $(BIN)

$(BIN): $(OBJ)
	$(CC) $^ -o $@

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@


