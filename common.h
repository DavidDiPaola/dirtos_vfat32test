#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>

#include <sys/types.h>
#include <unistd.h>

typedef uint8_t         u8;
typedef uint_fast8_t   u8f;
typedef int8_t          i8;
typedef int_fast8_t    i8f;
typedef uint16_t       u16;
typedef uint_fast16_t u16f;
typedef int16_t        i16;
typedef int_fast16_t  i16f;
typedef uint32_t       u32;
typedef uint_fast32_t u32f;
typedef int32_t        i32;
typedef int_fast32_t  i32f;
typedef _Bool         bool;
typedef size_t        size;
typedef ssize_t       ssize;

#define NULL ((void *)0)

#define lengthof(ptr) (sizeof(ptr) / sizeof(*(ptr)))

#define ptrto(type, exp) ((type *)&(exp))
#define ptratbyte(ptr, byte) &(((u8 *)(ptr))[byte])

#endif

