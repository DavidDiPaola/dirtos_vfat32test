#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>

#include <string.h>

#include "common.h"

i32f
utf8_copy(const char * src, size src_size, char * * out_dst, size * out_dst_size) {
	if (!src) {
		*out_dst      = NULL;
		*out_dst_size = 0;
		return 0;
	}

	char * dst = strndup(src, src_size);
	if (!dst) {
		return -1;
	}
	size dst_size = strlen(dst) + 1;

	*out_dst      = dst;
	*out_dst_size = dst_size;
	return 0;
}

/* the number of bytes in a string (excluding '\0', if present) */
size
utf8_length_b(const char * str, size str_size) {
	return strnlen(str, str_size);
}

i32f
utf8_concat(char * * inout_str0, size * inout_str0_size, const char * str1, size str1_size) {
	char * str0      = *inout_str0;
	size   str0_size = *inout_str0_size;

	size str0_length = utf8_length_b(str0, str0_size);
	size str1_length = utf8_length_b(str1, str1_size);
	str0_size = str0_length + str1_length + 1;
	str0 = realloc(str0, str0_size);
	if (!str0) {
		return -1;
	}
	strncpy(str0+str0_length, str1, str1_length);
	str0[str0_size-1] = '\0';

	*inout_str0      = str0;
	*inout_str0_size = str0_size;
	return 0;
}

i32f
utf8_concat_utf32_ch(char * * inout_str, size * inout_str_size, u32 ch) {
	char buffer[4];

	if (ch <= 0x7F) {
		buffer[0] = ch;
		return utf8_concat(inout_str, inout_str_size, buffer, 1);
	}
	else if (ch <= 0x07FF){
		buffer[0] = (0b110<<5) | (ch>>6);
		buffer[1] = (0b10<<6) | (ch&0b111111);
		return utf8_concat(inout_str, inout_str_size, buffer, 2);
	}
	else if (ch <= 0xFFFF){
		buffer[0] = (0b1110<<4) | (ch>>12);
		buffer[1] = (0b10<<6) | ((ch>>6)&0b111111);
		buffer[2] = (0b10<<6) | ((ch>>0)&0b111111);
		return utf8_concat(inout_str, inout_str_size, buffer, 3);
	}
	else {
		buffer[0] = (0b11110<<3) | (ch>>18);
		buffer[1] = (0b10<<6) | ((ch>>12)&0b111111);
		buffer[2] = (0b10<<6) | ((ch>> 6)&0b111111);
		buffer[3] = (0b10<<6) | ((ch>> 0)&0b111111);
		return utf8_concat(inout_str, inout_str_size, buffer, 4);
	}
}

i32f
utf8_concat_ucs2(char * * inout_str8, size * inout_str8_size, const u16 * str16, size str16_length) {
	for (size i=0; i<str16_length; i++) {
		u16 ch = str16[i];
		if ((ch==0x0000)||(ch==0xFFFF)) {
			break;
		}
		i32f status = utf8_concat_utf32_ch(inout_str8, inout_str8_size, ch);
		if (status < 0) {
			return -1;
		}
	}

	return 0;
}

i32f
utf8_compare(const char * str0, size str0_size, const char * str1, size str1_size) {
	if (!str0 && !str1) {
		return 0;
	}
	else if (!str0 && str1) {
		return -1;
	}
	else if (str0 && !str1) {
		return 1;
	}

	size min = str0_size < str1_size ? str0_size : str1_size;
	return strncmp(str0, str1, min);
}

